// Реалізувати функцію підрахунку факторіалу числа.
// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера число, яке введе користувач.
// - За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.


let numberUser = +prompt('Enter number: ');
console.log(numberUser)

function factorial(number){
    
    let result = 1;

    for(let i = 1; i <= number; i++){       
        result *= i;
    }
      
    alert(result);   
    
}

factorial(numberUser);

